package com.banarum.legaltechbot.MenuView

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.banarum.legaltechbot.BotView.BotActivity
import com.banarum.legaltechbot.R
import kotlinx.android.synthetic.main.activity_menu_view.*

/**
 * Created by Sergey on 25.11.2017.
 */

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_view)

        sign_in.setOnClickListener {
            val intent = Intent(this, BotActivity::class.java)
            startActivity(intent)
        }

        sign_up.setOnClickListener {
            val intent = Intent(this, BotActivity::class.java)
            startActivity(intent)
        }
    }
}