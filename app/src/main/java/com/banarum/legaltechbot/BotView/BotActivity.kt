package com.banarum.legaltechbot.BotView

import ai.api.AIServiceException
import ai.api.android.AIConfiguration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.banarum.legaltechbot.R
import ai.api.android.AIService
import ai.api.model.AIRequest
import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.activity_bot_view.*


class BotActivity : AppCompatActivity(), ActivityImpl {

    private lateinit var botListener:BotListener
    private lateinit var outputViewHandler:OutputViewHandler
    private lateinit var aiService:AIService

    var isListening = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bot_view)
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    listOf(Manifest.permission.RECORD_AUDIO).toTypedArray(), 19)
        }
        initBot(this)
    }

    private fun initBot(context: Context) {
        outputViewHandler = OutputViewHandler(this, list_view)
        botListener = BotListener(this, outputViewHandler, this)
        val config = AIConfiguration("3b1f44e106444e22a764a60453a082ef",
                ai.api.AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System)
        aiService = AIService.getService(context, config)
        aiService.setListener(botListener)
        chat_field.setOnEditorActionListener { textView, id, keyEvent ->
            if (id == EditorInfo.IME_ACTION_DONE) {
                send_btn.performClick()
                true
            }
            false
        }

        send_btn.setOnClickListener {
            val msg = chat_field.text.toString()
            outputViewHandler.sendMessage(MessageModel("You", msg))
            Thread{
                try {
                    botListener.onTextResult(aiService.textRequest(AIRequest(msg)))
                }catch (ex: AIServiceException){
                    ex.printStackTrace()
                }
            }.start()
            chat_field.text.clear()
        }

        listen_btn.setOnClickListener {
            if (isListening)
                aiService.stopListening()
            else
                aiService.startListening()
        }
    }

    override fun showStarted() {
        listen_btn.text = "Stop"
        isListening = true
    }

    override fun showStopped() {
        isListening = false
        listen_btn.text = "Listen"
    }

    override fun startRecording() {
        aiService.startListening()
    }
}