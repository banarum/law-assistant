package com.banarum.legaltechbot.BotView

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import java.util.*

/**
 * Created by Sergey on 25.11.2017.
 */
class SpeakerWrapper(context: Context) : TextToSpeech.OnInitListener {
    private var tts = TextToSpeech(context, this)

    private var callback:(()->Unit)? = null

    init{
        tts.setSpeechRate(0.9f)
        tts.setOnUtteranceProgressListener(object:UtteranceProgressListener(){
            override fun onDone(p0: String?) {
                callback?.invoke()
            }

            override fun onStart(p0: String?) {
            }

            override fun onError(utteranceId: String?) {

            }
        })
    }

    override fun onInit(p0: Int) {
        if (p0 == TextToSpeech.SUCCESS) {
            val result = tts.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported")
            }
        } else {
            Log.e("TTS", "Initilization Failed!")
        }
    }

    fun speak(msg:String, callback:(()->Unit)?){
        this.callback = callback
        tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null, "main")
    }
}