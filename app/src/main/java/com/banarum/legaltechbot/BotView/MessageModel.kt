package com.banarum.legaltechbot.BotView

data class MessageModel(
        val name:String,
        val text:String
)