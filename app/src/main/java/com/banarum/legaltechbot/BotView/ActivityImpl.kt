package com.banarum.legaltechbot.BotView

/**
 * Created by Sergey on 25.11.2017.
 */
interface ActivityImpl {
    fun startRecording()
    fun showStopped()
    fun showStarted()
}