package com.banarum.legaltechbot.BotView

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView

class OutputViewHandler(context:Context, private var outputView: RecyclerView) {

    private var adapter:ChatAdapter

    init{
        adapter = ChatAdapter()
        outputView.layoutManager = LinearLayoutManager(context)
        outputView.adapter = adapter
    }

    fun sendMessage(message:MessageModel){
        val arr = mutableListOf<MessageModel>()
        for (item in adapter.data){
            arr.add(item)
        }
        arr.add(MessageModel(message.name, message.text))
        adapter.data = arr
        adapter.notifyItemInserted(arr.size-1)
    }
}