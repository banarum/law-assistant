package com.banarum.legaltechbot.BotView

import ai.api.AIListener
import ai.api.model.AIError
import ai.api.model.AIResponse
import android.content.Context
import android.os.Handler

/**
 * Created by Sergey on 25.11.2017.
 */

class BotListener(context: Context, private var outputViewHandler: OutputViewHandler, private var activityImpl: ActivityImpl) : AIListener {

    private val handler = Handler(context.mainLooper)
    private val speakerWrapper = SpeakerWrapper(context)

    override fun onListeningStarted() {
        activityImpl.showStarted()
    }

    override fun onError(error: AIError?) {
        activityImpl.showStopped()
    }

    override fun onAudioLevel(level: Float) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onListeningCanceled() {
        activityImpl.showStopped()
    }

    override fun onListeningFinished() {
        activityImpl.showStopped()
    }

    fun onTextResult(result: AIResponse?) {
        if (result != null) {
            val speech = result.result.fulfillment.speech
            val messageOut = MessageModel("Bot", speech)
            handler.post {
                speakerWrapper.speak(messageOut.text, null)
                outputViewHandler.sendMessage(messageOut)
            }
        }
    }

    override fun onResult(result: AIResponse?) {
        if (result != null) {
            val speech = result.result.fulfillment.speech
            val source = result.result.resolvedQuery
            val messageIn = MessageModel("You", source)
            val messageOut = MessageModel("Bot", speech)
            handler.post {
                outputViewHandler.sendMessage(messageIn)
                speakerWrapper.speak(messageOut.text, {
                    if (!messageOut.text.contains("enter", true))
                        handler.post {
                            activityImpl.startRecording()
                        }
                })
                outputViewHandler.sendMessage(messageOut)
            }
        }

    }
}