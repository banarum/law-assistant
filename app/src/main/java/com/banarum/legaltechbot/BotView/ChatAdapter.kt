package com.banarum.legaltechbot.BotView

import android.graphics.Color
import android.support.v7.widget.CardView
import android.util.Log
import android.view.View
import android.widget.TextView
import com.banarum.legaltechbot.R
import ru.appkode.baseui.recycler_view.adapters.ListAdapter

class ChatAdapter : ListAdapter<MessageModel, ChatAdapter.ViewHolder>(R.layout.item_chat) {

    override fun createViewHolder(itemView: View): ViewHolder {
        return ViewHolder(itemView)
    }

    override fun bindViewHolder(holder: ViewHolder, item: MessageModel) {
        holder.msgText.text = item.text
        if (item.name == "Bot") {
            holder.msgText.setTextColor(Color.WHITE)
            holder.card.setCardBackgroundColor(Color.parseColor("#F09B00"))
        }else{
            holder.msgText.setTextColor(Color.GRAY)
            holder.card.setCardBackgroundColor(Color.WHITE)
        }
    }

    inner class ViewHolder(view: View) : ListAdapter.ViewHolder(view) {
        override val clickTarget = null
        val msgText = view.findViewById<TextView>(R.id.txt)
        val card = view.findViewById<CardView>(R.id.card)
    }
}